from django.conf.urls import patterns, include, url

urlpatterns = patterns('Notifications.views',
   url(r'^notifications/(?P<user_id>\d+)/', 'notifications', name='notifications'),
   url(r'^messages/(?P<user_id>\d+)/', 'messages', name='messages'),
   url(r'^notification/(?P<user_id>\d+)/(?P<notification_id>\d+)/', 'notification', name='notification'),
   url(r'^message/(?P<user_id>\d+)/(?P<message_id>\d+)/', 'message', name='message'),
   url(r'^message/create/(?P<user_id>\d+)/', 'create_message', name='create_message'),
   
)
