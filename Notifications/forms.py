from __future__ import absolute_import

from django import forms

from .models import Messages

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

class MessageForm(forms.ModelForm):
    
    class Meta:
        model = Messages
        fields = ('recipient', 'message',)
    
    
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        
        self.helper.add_input(Submit('submit', 'Submit'))
        super(MessageForm, self).__init__(*args, **kwargs)
        