from __future__ import absolute_import

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from accounts.models import User

from friendship.signals import friendship_request_accepted
from event.signals import group_request_accepted, group_event, event_discussion, group_discussion
from event.models import GroupMember

import datetime



class Notifications(models.Model):
    user = models.ForeignKey(User)
    message = models.CharField(max_length=255)
    created = models.DateTimeField(default=datetime.date.today())
    viewed = models.BooleanField(default=False)
    expiration_date = models.DateTimeField(default=datetime.datetime.today() + datetime.timedelta(30))


@receiver(friendship_request_accepted)
def friendship_accepted(sender, **kwargs):
    Notifications.objects.create(user=kwargs.get("from_user"),
        message="%s has accepted your friend request." % kwargs.get("to_user"))
 
@receiver(group_request_accepted)
def group_request(sender, **kwargs):
    Notificaiton.objects.create(user=kwargs.get("to_user"),
        message="Your group request for %s has been accepted." % kwargs.get("group"))

@receiver(group_event)
def group_event_notifcation(sender, **kwargs):

    group_users = [entry.user for entry in GroupMember.objects.filter(group=kwargs.get("group"))]
    
    for user in group_users:
         Notificaiton.objects.create(user=user,
             message="New group event posted in %s." % kwargs.get("group"))
 

@receiver(event_discussion)
def event_discussion_notification(sender, **kwargs):

    event_users = [entry.user for entry in EventMember.objects.filter(event=kwargs.get("event"))]
    
    for user in event_users:
        Notificaiton.objects.create(user=user,
            message="%s has commented in %s." % (kwargs.get("from_user"), kwargs.get("event")))
        
    
@receiver(group_discussion)
def group_discussion_notification(sender, **kwargs):

    group_users = [entry.user for entry in GroupMember.objects.filter(group=kwargs.get("group"))]
    Notificaiton.objects.create(user=kwargs.get("to_user"),
        message="%s has posted in %s" % (kwargs.get('from_user'), kwargs.get("group")))


                                


    
    
    
    
