from __future__ import absolute_import

from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.core.context_processors import csrf
from django.http import HttpResponseRedirect
from django.utils import timezone

from .models import Notifications


def notifications(request, user_id):
    
    notificationList = Notifications.objects.filter(user=user_id)
    
    for n in notificationList:
        if n.viewed == True or n.expiration_date < timezone.now():
            delete_n = Notifications.objects.get(id=n.id)
            delete_n.delete()
    
    args= {}
    args.update(csrf(request))
    
    args['notifications'] = notificationList
    
    return render(request, 'notifications.html', args)



def notification(request, user_id, notification_id):
    
    n = Notifications.objects.get(id=notification_id)
    n.viewed = True
    n.save()
    
    args = {}
    args.update(csrf(request))
    
    args['notification'] = n.message
    
    return render(request, 'notification.html', args)
    
