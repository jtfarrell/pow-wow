# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('Notifications', '0002_auto_20150527_1240'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notifications',
            name='created',
            field=models.DateTimeField(default=datetime.date(2015, 6, 7)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notifications',
            name='expiration_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 7, 20, 49, 34, 376096)),
            preserve_default=True,
        ),
    ]
