# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('Notifications', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='messages',
            name='sender',
        ),
        migrations.DeleteModel(
            name='Messages',
        ),
        migrations.AlterField(
            model_name='notifications',
            name='created',
            field=models.DateTimeField(default=datetime.date(2015, 5, 27)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notifications',
            name='expiration_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 26, 12, 40, 39, 671696)),
            preserve_default=True,
        ),
    ]
