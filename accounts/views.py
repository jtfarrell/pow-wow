from __future__ import absolute_import

from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.formtools.wizard.views import SessionWizardView
from django.core.files.storage import FileSystemStorage, default_storage
from django.core.urlresolvers import reverse
from django.core.context_processors import csrf
from django.core.mail import send_mail
from django.utils.decorators import method_decorator
from django.conf import settings


from .forms import *
from .models import *

import hashlib, datetime, random

User = get_user_model()

def register(request):

    '''Registers user and signs in automatically'''
    
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
        
    else:
        args = {}
        args.update(csrf(request))
        if request.method == 'POST':
            form = RegistrationForm(request.POST)
            args['form'] = form
            if form.is_valid():
                sign_in = form.save() 
                form.save_m2m()   
                sign_in = auth.authenticate(email=request.POST['email'],
											password=request.POST['password1'])
										
                auth.login(request, sign_in)    
			 
                return HttpResponseRedirect(reverse('user_creation'))
	
        else:
            args['form'] = RegistrationForm()
		
        return render(request, 'register.html', args)
    
class UserCreationWizard(SessionWizardView):
    instance = None
    template_name = 'usercreation.html'
    file_storage = default_storage
    
    @method_decorator(user_passes_test(lambda u: not u.is_active))
    def dispatch(self, *args, **kwargs):
        return super(UserCreationWizard, self).dispatch(*args, **kwargs)
    
    
    def get_form_instance(self, step):
        
        if self.instance == None:
            self.instance = UserProfile()
        
        return self.instance
    
    def done(self, form_list, **kwargs):
    
        user = self.request.user
        
        email = self.request.user.email
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]            
        confirmation_key = hashlib.sha1(salt+email).hexdigest()
        self.instance.confirmation_key = confirmation_key
        self.instance.user = self.request.user
        self.instance.save()

        
        email_subject = '[Pow Wow] Confirm your email address'
        email_body = "Hey! thanks for joining Powwow! You just need to click \
					  the link to confirm that we've got the right email address. \
					  http://127.0.0.1:8000/register_confirm/%s" % confirmation_key
		# local URL -- change once deployed
        send_mail(email_subject, email_body, 'james.t.farrell91@gmail.com',
				 [email], fail_silently=False)
		
        user.is_active = True
        user.name = UserProfile.objects.get(user=self.request.user).first_name + " " + \
         UserProfile.objects.get(user=self.request.user).last_name
        user.save()
        
        
		
        return HttpResponseRedirect(reverse('home'))


def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    else:
        if request.method == 'POST':
			email = request.POST.get('email', '')
			password = request.POST.get('password', '')
			user = auth.authenticate(email=email, password=password)
			
			if user is not None and user.is_active:
			    auth.login(request, user)
			    return HttpResponseRedirect('/')
			else:
			    messages.add_message(request, settings.DANGER_MESSAGE, "Email or\
			     password invalid.")
			    return render(request, 'login.html')
        else:
            c = {}
            c.update(csrf(request))
            return render_to_response('login.html', c)


def register_confirm(request, confirmation_key):
    if request.user.is_authenticated():
        HttpResponseRedirect('/')
        
    user_profile = get_object_or_404(UserProfile, confirmation_key=confirmation_key)
    
    user = user_profile.email_confirmation = True
    user.save()
    messages.add_message(request, messages.SUCCESS, "You can create and join events.")
    return render(request, 'home.html')
    


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/login/')
    

def confirmation_email(email, key):
    email_subject = '[Pow Wow] Confirm your email address'
    email_body = "Hey! thanks for joining Powwow! You just need to click \
                          the link to confirm that we've got the right email address. \
                          http://127.0.0.1:8000/register_confirm/%s" % key
            # local URL -- change once deployed
    send_mail(email_subject, email_body, 'james.t.farrell91@gmail.com',
             [email], fail_silently=False)

    
    
