# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_remove_userprofile_interest'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='interests',
            field=models.ManyToManyField(to='accounts.Interests'),
            preserve_default=True,
        ),
    ]
