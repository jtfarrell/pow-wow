# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import accounts.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_user_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=300)),
                ('last_name', models.CharField(max_length=300)),
                ('date_of_birth', models.DateField()),
                ('profile_pic', models.FileField(default=b'assets/profile_pic_files/default.pgn', upload_to=accounts.models.get_upload_file_name)),
                ('bio', models.TextField(blank=True)),
                ('email_confirmation', models.BooleanField(default=False)),
                ('confirmation_key', models.CharField(max_length=40, blank=True)),
                ('interest', models.ManyToManyField(to='accounts.Interests')),
                ('location', models.ForeignKey(to='accounts.Locations')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
