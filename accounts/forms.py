from __future__ import absolute_import

from django import forms
from django.forms import extras

from .models import User, UserProfile

from datetime import date

valid_dob_years = [i for i in xrange((date.today().year - 120), date.today().year)]


class RegistrationForm(forms.ModelForm):
    """
    Form for registering a new account.
    """
    email = forms.EmailField(widget=forms.TextInput,label="Email")
    password1 = forms.CharField(widget=forms.PasswordInput,
                                label="Password")
    password2 = forms.CharField(widget=forms.PasswordInput,
                                label="Password Confirmation")

    class Meta:
        model = User
        fields = ['email', 'password1', 'password2', 'interests']

    def __init__(self, *args, **kwargs):
        """
        Overriding form attributes.
        """
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['placeholder'] = u'jsmith@xxxx.com'
        self.fields['password1'].widget.attrs['placeholder'] = u'*******'
        self.fields['password2'].widget.attrs['placeholder'] = u'*******'
    
    def clean(self):
        """
        Verifies that the values entered into the password fields match

        NOTE: Errors here will appear in ``non_field_errors()`` because it applies to more than one field.
        """
        cleaned_data = super(RegistrationForm, self).clean()
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError("Passwords don't match. Please enter both fields again.")
        return self.cleaned_data

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user
        
class UserProfileForm1(forms.ModelForm):
    
    date_of_birth = forms.DateField(widget=extras.SelectDateWidget(years= valid_dob_years))
    
    class Meta:
        model = UserProfile
        fields = ('first_name', 'last_name', 'date_of_birth',)
    
    
class UserProfileForm2(forms.ModelForm):
    
    class Meta:
        model = UserProfile
        fields = ('location',)

class UserProfileForm3(forms.ModelForm):
    
    class Meta:
        model = UserProfile
        fields = ('bio', 'profile_pic',)



        
