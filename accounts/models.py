from __future__ import absolute_import

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.conf import settings

from powwow import settings

from time import time


def get_upload_file_name(instance, filename):
    return "profile_pic_files/%s_%s" % (str(time()).replace('.','_'), filename)


class Locations(models.Model):
    locations = models.CharField(max_length=300)
    
    def __str__(self):
        return self.locations

class Interests(models.Model):
    interests = models.CharField(max_length=300)
    
    def __str__(self):
        return self.interests
        

class UserManager(BaseUserManager):

    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Users must have an email address')
        
        user = self.model(
            email=UserManager.normalize_email(email)
            )
            
        user.set_password(password)
        user.save(using=self._db)
        
        return user
    
    def create_superuser(self, email, password):
        user = self.create_user(email,
                                password=password
                                )
        
        user.is_admin = True
        user.save(using=self._db)
        
        return user


class User(AbstractBaseUser):
    
    email = models.EmailField(max_length=254, unique=True, db_index=True)
    name = models.CharField(max_length=254, default='user', blank=True)
    interests = models.ManyToManyField(Interests)
    joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    
    objects = UserManager()

    USERNAME_FIELD = 'email'
    
    def get_full_name(self):
        return self.name
    
    def get_short_name(self):
        return self.email
    
    def __unicode__(self):
        return self.email
 
    def has_perm(self, perm, obj=None):
        # Handle whether the user has a specific permission?"
        return True
 
    def has_module_perms(self, accounts):
        # Handle whether the user has permissions to view the app `app_label`?"
        return True
 
    @property
    def is_staff(self):
        # Handle whether the user is a member of staff?"
        return self.is_admin

    def __str__(self):
        return self.email
        


class UserProfile(models.Model):

    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    first_name = models.CharField(max_length=300)
    last_name = models.CharField(max_length=300)
    date_of_birth = models.DateField()
    profile_pic = models.FileField(upload_to=get_upload_file_name, default='assets/profile_pic_files/default.pgn')
    bio = models.TextField(blank=True)
    email_confirmation = models.BooleanField(default=False)
    confirmation_key = models.CharField(max_length=40, blank=True)
    location = models.ForeignKey(Locations)

    def __str__(self):
        return self.location
    
    
