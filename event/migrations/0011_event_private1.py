# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0010_auto_20150417_1812'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='private1',
            field=models.NullBooleanField(),
            preserve_default=True,
        ),
    ]
