# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0003_auto_20150414_1650'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='private1',
            field=models.NullBooleanField(),
            preserve_default=True,
        ),
    ]
