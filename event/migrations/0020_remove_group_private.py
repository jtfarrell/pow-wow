# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0019_group_private1'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='group',
            name='private',
        ),
    ]
