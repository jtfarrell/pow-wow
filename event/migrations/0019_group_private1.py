# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0018_groupreview_group'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='private1',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
