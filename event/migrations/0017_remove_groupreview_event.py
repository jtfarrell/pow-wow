# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0016_auto_20150429_2003'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='groupreview',
            name='event',
        ),
    ]
