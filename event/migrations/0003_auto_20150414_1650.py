# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0002_auto_20150411_1218'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='private',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
