# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import event.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts', '0002_user_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('details', models.TextField()),
                ('thumbnail', models.FileField(upload_to=event.models.get_upload_file_name, blank=True)),
                ('pub_date', models.DateTimeField()),
                ('start_date_and_time', models.DateTimeField()),
                ('address', models.CharField(max_length=200)),
                ('maximum_people', models.IntegerField()),
                ('activity', models.ForeignKey(to='accounts.Interests')),
                ('location', models.ForeignKey(to='accounts.Locations')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('review', models.TextField()),
                ('pub_date', models.DateTimeField()),
                ('rating', models.IntegerField()),
                ('event', models.ForeignKey(to='event.Event')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
