# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import event.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts', '0002_user_name'),
        ('event', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('about', models.TextField()),
                ('thumbnail', models.FileField(upload_to=event.models.get_upload_file_name, blank=True)),
                ('pub_date', models.DateTimeField()),
                ('private', models.BooleanField()),
                ('activity', models.ForeignKey(to='accounts.Interests')),
                ('location', models.ForeignKey(to='accounts.Locations')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GroupEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('about', models.TextField()),
                ('thumbnail', models.FileField(upload_to=event.models.get_upload_file_name, blank=True)),
                ('pub_date', models.DateTimeField()),
                ('start_date_and_time', models.DateTimeField()),
                ('location', models.CharField(max_length=253)),
                ('address', models.CharField(max_length=200)),
                ('group', models.ForeignKey(to='event.Group')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GroupReview',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('review', models.TextField()),
                ('pub_date', models.DateTimeField()),
                ('rating', models.IntegerField()),
                ('event', models.ForeignKey(to='event.GroupEvent')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RenameField(
            model_name='event',
            old_name='details',
            new_name='about',
        ),
        migrations.RenameField(
            model_name='event',
            old_name='maximum_people',
            new_name='max_people',
        ),
    ]
