# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0009_event_private'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='max_people',
            field=models.IntegerField(blank=True),
            preserve_default=True,
        ),
    ]
