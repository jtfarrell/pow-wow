# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0020_remove_group_private'),
    ]

    operations = [
        migrations.RenameField(
            model_name='group',
            old_name='private1',
            new_name='private',
        ),
    ]
