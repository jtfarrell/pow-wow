# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0017_remove_groupreview_event'),
    ]

    operations = [
        migrations.AddField(
            model_name='groupreview',
            name='group',
            field=models.ForeignKey(default='', to='event.Group'),
            preserve_default=False,
        ),
    ]
