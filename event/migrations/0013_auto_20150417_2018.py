# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0012_remove_event_private'),
    ]

    operations = [
        migrations.RenameField(
            model_name='event',
            old_name='private1',
            new_name='private',
        ),
    ]
