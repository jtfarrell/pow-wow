# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_user_name'),
        ('event', '0007_remove_group_activity'),
    ]

    operations = [
        migrations.AddField(
            model_name='groupevent',
            name='activity',
            field=models.ForeignKey(default='', to='accounts.Interests'),
            preserve_default=False,
        ),
    ]
