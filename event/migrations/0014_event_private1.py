# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0013_auto_20150417_2018'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='private1',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
