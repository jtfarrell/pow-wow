from django.dispatch import Signal

group_request_accepted = Signal(providing_args=['group', 'to_user'])
group_event = Signal(providing_args=['group'])
event_discussion = Signal(providing_args=['event', 'from_user'])
group_discussion = Signal(providing_args=['group', 'from_user'])