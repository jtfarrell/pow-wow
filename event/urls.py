from __future__ import absolute_import

from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required

from .forms import EventForm1, EventForm2
from .views import CreateEventWizard



urlpatterns = patterns('event.views',
    url(r'^create_group/$', 'create_group', name='create_group'),
    url(r'^(?P<group_id>\d+)-(?P<group_name>[-\w]+)/', 'group', name='group'),
    url(r'^create_group_event/(?P<group_id>\d+)/$', 'create_group_event', name='create_group_event'),
    url(r'^group_event/(?P<group_event_id>\d+)-(?P<group_event_name>[-\w]+)/', 'group_event', name="group_event"),
    url(r'^create_event/$', login_required(CreateEventWizard.as_view([EventForm1, EventForm2])), name='create_event'),
    url(r'^event/(?P<event_id>\d+)-(?P<event_name>[-\w]+)/', 'event', name='event'),
    url(r'^group_review/(?P<group_id>\d+)/$', 'group_review', name='group_review'),
    url(r'^event_review/(?P<event_id>\d+)/$', 'event_review', name='event_review'),
    url(r'^event_feed/$', 'users_event_feed', name='users_event_feed'),
    url(r'^join_group/(?P<group_id>\d+)/', 'join_group', name="join_group"),
)