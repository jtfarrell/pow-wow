from __future__ import absolute_import

from django.shortcuts import render, render_to_response
from django.core.urlresolvers import reverse
from django.core.files.storage import default_storage
from django.core.context_processors import csrf
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth import get_user_model
from django.contrib.formtools.wizard.views import SessionWizardView
from django.utils.decorators import method_decorator
from django.utils import timezone

from .models import Group, GroupEvent, Event, GroupReview, Review, GroupMember, EventMember, GroupRequest
from .forms import GroupForm, GroupEventForm, GroupReviewForm, ReviewForm
from accounts.models import UserProfile
from Notifications.models import Notifications
from powwow import settings

User = get_user_model()

@login_required
def create_group(request):
    """
    Create a group which user can join. Can be made private.
    """
    email_confirmation = UserProfile.objects.get(user=request.user).email_confirmation
    
    if request.POST:
        form = GroupForm(request.POST, request.FILES)
        
        if form.is_valid():
            save_form = form.save(commit=False)
            save_form.user = request.user
            save_form.pub_date = timezone.now()
            save_form.save()
            
            group_info = Group.objects.get(user=request.user, pub_date=save_form.pub_date)
            name = group_info.title.replace(' ', '-')
            messages.add_message(request, messages.INFO, "The group '%s' was successfully\
             created." % group_info.title)
            
            GroupMember.objects.create(user=request.user, group=group_info)
            
            return HttpResponseRedirect(reverse('event:group', kwargs={
                    'group_id': group_info.id,
                    'group_name': name}))
    
    else:
        form = GroupForm()
    
    args = {}
    args.update(csrf(request))
    args['form'] = form
    
    return render(request, 'create_group.html', args)
        
@login_required
def group(request, group_id, group_name):
    
    rating_list = []
    group_info = Group.objects.get(id=group_id)
    name = UserProfile.objects.get(user=group_info.user)
    group_ratings = GroupReview.objects.filter(group=group_id)
    group_events = GroupEvent.objects.filter(group=group_id)
    
    for ratings in group_ratings:
        rating_list.append(ratings.rating)
        
    if len(rating_list) == 0:
        rating_list.append(1)
    
    args = {}
    args.update(csrf(request))
    args['group_info'] = group_info
    args['name'] = name
    args['average_rating'] = sum(rating_list) // len(rating_list)
    args['events'] = group_events
    
    if group_info.private == True:
        if request.user == group_info.user or GroupMember.objects.get(group=group_id, user=request.user).exists():
            return render(request, 'group.html', args)
        
        else:
            return render(request, 'private_group.html')
    
    return render(request, 'group.html', args)
    

def join_group(request, group_id):
    
    group = Group.objects.get(id=group_id)
    name = group.title.replace(' ', '-')
    
    if GroupMember.objects.filter(group=group, user=request.user).exists() == False:
        if group.private == False:
            GroupRequest.objects.create(
                from_user=request.user,
                to_user=group.user,
                group=group
            )
            messages.add_message(request, messages.SUCCESS, "Group request sent.")
            
            Notifications.objects.create(
                user=group.user,
                message="%s wants to join the group %s." % (request.user.name, group.title)
            )
            
            return HttpResponseRedirect(reverse('event:group', kwargs={
                'group_id': group_id,
                'group_name': name
            }))
            
    
    else:
        return HttpResponseRedirect(reverse('event:group', kwargs={
                'group_id': group_id,
                'group_name': name
            }))
    
        
        
    
    



def create_group_event(request, group_id):

    group_info = Group.objects.get(id=group_id)
    
    if request.user == group_info.user:
        if request.POST:
            form = GroupEventForm(request.POST, request.FILES)
            
            if form.is_valid():
                save_form = form.save(commit=False)
                save_form.pub_date = timezone.now()
                save_form.location = group_info.location
                save_form.group = group_info
                save_form.save()
                
                url_args = GroupEvent.objects.get(group=group_info, pub_date=save_form.pub_date)
                name = url_args.title.replace(' ', '-')
                messages.add_message(request, messages.INFO, "The event '%s' was successfully\
				 created." % url_args.title)
                return HttpResponseRedirect(reverse('event:group_event', kwargs={
                    'group_event_id': url_args.id,
                    'group_event_name': name}))
            
            
	
        else:
            form = GroupEventForm()
            form.helper.form_action = reverse('event:create_group_event', kwargs={'group_id': group_id})
	
        args = {}
        args.update(csrf(request))
        args['form'] = GroupEventForm()
        args['group_info'] = group_info
	
        return render(request, 'create_group_event.html', args)
	
    else:
        return render(request, 'private_group.html')

    
def group_event(request, group_event_id, group_event_name):
    
    event = GroupEvent.objects.get(id=group_event_id)
    
    args = {}
    args.update(csrf(request))
    args['event'] = event
    
    if event.group.private == True:
        if request.user == event.group.user:
            return render(request, 'group_event.html', args)
        
        else:
            return render(request, 'private_group.html')
    
    return render(request, 'group_event.html', args)

        

class CreateEventWizard(SessionWizardView):
    instance = None
    template_name = 'create_event.html'
    file_storage = default_storage

    def get_form_instance( self, step ):
        if self.instance is None:
            self.instance = Event()
        return self.instance

    def done( self, form_list, **kwargs ):
        self.instance.user = self.request.user
        self.instance.pub_date = timezone.now()
        self.instance.save()
        
        url_args = Event.objects.get(user=self.request.user, pub_date=self.instance.pub_date)
        name = url_args.title.replace(' ', '-')
    
        return HttpResponseRedirect(reverse('event:event', kwargs={
            'event_id': url_args.id,
            'event_name': name
            }))

def event(request, event_id, event_name):
    
    event = Event.objects.get(id=event_id)
    
    args = {}
    args.update(csrf(request))
    args['event'] = event
    

    
    return render(request, 'event.html', args)

@login_required
def group_review(request, group_id):

    group_info = Group.objects.get(id=group_id)
    
    if request.user == group_info.user:
        if request.POST:
            form = GroupReviewForm(request.POST)
            if form.is_valid():
                save_form = form.save(commit=False)
                save_form.pub_date = timezone.now()
                save_form.group = group_info
                save_form.user = request.user
                save_form.save()
                
                name = group_info.title.replace(' ', '-')
                messages.add_message(request, messages.INFO, "Your Review has been added.")
                return HttpResponseRedirect(reverse('event:group', kwargs={
                    'group_id': group_id,
                    'group_name': name}))
                    
        else:
            form = GroupReviewForm()
            
	
        args = {}
        args.update(csrf(request))
        args['form'] = form
        args['group_info'] = group_info
	
        return render(request, 'group_review.html', args)
	
    else:
        return render(request, 'private_group.html')
    
    
@login_required
def event_review(request, event_id):
    
    event_info = Event.objects.get(id=event_id)
    
    if request.user == event_info.user:
        if request.POST:
            form = ReviewForm(request.POST)
            if form.is_valid():
                save_form = form.save(commit=False)
                save_form.pub_date = timezone.now()
                save_form.event = event_info
                save_form.user = request.user
                save_form.save()
                
                name = event_info.title.replace(' ', '-')
                messages.add_message(request, messages.INFO, "Your Review has been added.")
                return HttpResponseRedirect(reverse('event:event', kwargs={
                    'event_id': event_id,
                    'event_name': name}))
                    
        else:
            form = ReviewForm()
	
        args = {}
        args.update(csrf(request))
        args['form'] = form
        args['event_info'] = event_info
	
        return render(request, 'review.html', args)
	
    else:
        return render(request, 'private_group.html')
            

@login_required
def users_event_feed(request):
    """
    This method will display activities near to the user's location
    and is interested in.
    
    will also show popular activities in area and most recent in the
    sidebar.
    """
    Interests = []
    user_genres = User.objects.get(id=request.user.id)
    genres = user_genres.interests.all()
    
    for items in genres:
        Interests.append(items)
        
    User_info = UserProfile.objects.get(user=request.user)
    GroupEvents = GroupEvent.objects.filter(location=User_info.location, activity__in=Interests)
    Events = Event.objects.filter(location=User_info.location, activity__in=Interests)
    
    args = {}
    args.update(csrf(request))
    
    args['events'] = Events
    args['groups'] = GroupEvents
    
    return render(request, 'user_event_feed.html', args)
    
    
                                                                                      
    
