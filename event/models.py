from __future__ import absolute_import

from django.db import models
from django.conf import settings
from powwow import settings
from time import time

from .signals import group_request_accepted, group_event, event_discussion, group_discussion                    
from accounts.models import Locations, Interests, User




def get_upload_file_name(instance, filename):
    return "profile_pic_files/%s_%s" % (str(time()).replace('.','_'), filename)


class Group(models.Model):
    user = models.ForeignKey(User)
    title = models.CharField(max_length=200)
    about = models.TextField()
    thumbnail = models.FileField(upload_to=get_upload_file_name, blank=True)
    pub_date = models.DateTimeField()
    location = models.ForeignKey(Locations)
    private = models.BooleanField(default=False)

class GroupEvent(models.Model):
    group = models.ForeignKey(Group)
    title = models.CharField(max_length=200)
    about = models.TextField()
    thumbnail = models.FileField(upload_to=get_upload_file_name, blank=True)
    pub_date = models.DateTimeField()
    start_date_and_time = models.DateTimeField()
    location = models.CharField(max_length=253)
    address = models.CharField(max_length=200)
    activity = models.ForeignKey(Interests)
    
    def signal(self):
		group_event.send(
			group=self.group
		)
    

class Event(models.Model):
    user = models.ForeignKey(User)
    title = models.CharField(max_length=200)
    about = models.TextField()
    thumbnail = models.FileField(upload_to=get_upload_file_name, blank=True)
    pub_date = models.DateTimeField()
    start_date_and_time = models.DateTimeField()
    activity = models.ForeignKey(Interests)
    location = models.ForeignKey(Locations)
    address = models.CharField(max_length=200)
    max_people = models.IntegerField(blank=True)
    


class GroupReview(models.Model):
    user = models.ForeignKey(User)
    group = models.ForeignKey(Group)
    review = models.TextField()
    pub_date = models.DateTimeField()
    rating = models.IntegerField()
    
    


class Review(models.Model):
    user = models.ForeignKey(User)
    event = models.ForeignKey(Event)
    review = models.TextField()
    pub_date = models.DateTimeField()
    rating = models.IntegerField()


class GroupRequest(models.Model):
    from_user = models.ForeignKey(User)
    to_user = models.ForeignKey(User, related_name="sender")
    group = models.ForeignKey(Group)
    
    def __unicode__(self):
        return "%s wants to join %s" % (self.from_user, self.group)
    
    def accept(self):
        """
        Accepts group request.
        """
        GroupMember.objects.create(
            user=self.from_user,
            group=self.group
        )
        
        group_request_accepted.send(
            group=self.group,
            to_user=self.from_user
        )
        
        self.delete()
        
        return True
    
    def reject(self):
        """
        Rejects group request.
        """
        self.delete()
        
        return True
        


class GroupMember(models.Model):
    user = models.ForeignKey(User)
    group = models.ForeignKey(Group)
    

class EventMember(models.Model):
    user = models.ForeignKey(User)
    event = models.ForeignKey(Event)


class GroupDiscussion(models.Model):
    group = models.ForeignKey(Group)
    user = models.ForeignKey(User)
    comment = models.CharField(max_length=255, blank=False)
    pub_date = models.DateTimeField()
    
    def __unicode__(self):
        return "%s posted in %s." % (self.user, self.group)
    
    def signal(self):
		group_discussion.send(
			group=self.group,
			from_user=self.user
		)

class EventDiscussion(models.Model):
    event = models.ForeignKey(Event)
    user = models.ForeignKey(User)
    comment = models.CharField(max_length=255, blank=False)
    pub_date = models.DateTimeField()
    
    def __unicode__(self):
        return "%s posted in %s." % (self.user, self.event)
    
    def signal(self):
        event_discussion.send(
            event=self.event,
            from_user=self.user
        )
    
    

    


    
    
    
    
    
