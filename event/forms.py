from __future__ import absolute_import

from django import forms

from datetimewidget.widgets import DateTimeWidget

from .models import Group, GroupEvent, Event, GroupReview, Review

import datetime
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from crispy_forms.layout import Layout
from crispy_forms.bootstrap import TabHolder, Tab

calender_start_date = str(datetime.date.today())
helper = FormHelper()


class GroupForm(forms.ModelForm):
    
    class Meta:
        model = Group
        exclude = ('user', 'pub_date',)
        
        
    
class GroupEventForm(forms.ModelForm):
    class Meta:
        model = GroupEvent
        exclude = ('group', 'location', 'pub_date',)
        
        DateTimeOptions = {
            'format': 'dd/mm/yyyy HH:ii:ss',
			'autoclose': True,
			'startDate': calender_start_date,
        }
        
        widgets = {
            #Use localization and bootstrap 3
            'start_date_and_time': DateTimeWidget(options = DateTimeOptions)
        }
        
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.add_input(Submit('submit', 'Submit'))
        super(GroupEventForm, self).__init__(*args, **kwargs)


class EventForm1(forms.ModelForm):
    
    class Meta:
        model = Event
        fields = ('activity', 'title', 'about', 'thumbnail',)

class EventForm2(forms.ModelForm):
    
    class Meta:
        model = Event
        fields = ('start_date_and_time', 'location', 'address', 'max_people',)
		
        DateTimeOptions = {
            'format': 'dd/mm/yyyy HH:ii:ss',
			'autoclose': True,
			'showMeridian' : True,
			'startDate': calender_start_date,
        }
        
        widgets = {
            #Use localization and bootstrap 3
            'start_date_and_time': DateTimeWidget(options = DateTimeOptions)
        }
        
    

class GroupReviewForm(forms.ModelForm):
    
    class Meta:
        model = GroupReview
        fields = ('rating', 'review',)
    
    def __init__(self, *args, **kwargs):
        super(GroupReviewForm, self).__init__(*args, **kwargs)
        self.fields['rating'].widget.attrs['class'] = 'rating'
        self.fields['rating'].widget.attrs['data-clearable'] = 'remove'
        self.fields['rating'].widget.attrs['data-max'] = '4'
        



class ReviewForm(forms.ModelForm):
    
    class Meta:
        model = Review
        fields = ('rating', 'review',)
        
    def __init__(self, *args, **kwargs):
        super(ReviewForm, self).__init__(*args, **kwargs)
        self.fields['rating'].widget.attrs['class'] = 'rating'
        self.fields['rating'].widget.attrs['data-clearable'] = 'remove'
        self.fields['rating'].widget.attrs['data-max'] = '4'
        
        
