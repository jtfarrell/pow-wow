from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required

from home.forms import ContactForm1, ContactForm2
from home.views import ContactWizard
from accounts.forms import UserProfileForm1, UserProfileForm2, UserProfileForm3
from accounts.views import UserCreationWizard

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^event/', include('event.urls', namespace='event', app_name='event')),
    url(r'^notification/', include('Notifications.urls', namespace='notification', app_name='notification')),
    url(r'^friends/', include('Friendships.urls', namespace='friendship', app_name='friendship')),
    url(r'^messages/', include('django_messages.urls')), 
    url(r'^$', 'home.views.home', name="home"),
    url(r'^user/(?P<user_id>\d+)/', 'home.views.users', name="users"),
    url(r'^register/', ('accounts.views.register'), name="register"),
    url(r'^register_confirm/(?P<confirmation_key>\w+)/', ('accounts.views.register_confirm'), name="register_confirm"),
    url(r'^login/', ('accounts.views.login'), name="login"),
    url(r'^logout/', ('accounts.views.logout'), name="logout"),
    url(r'^user_creation/$', login_required(UserCreationWizard.as_view([UserProfileForm1, UserProfileForm2, UserProfileForm3])), name="user_creation"),
    url(r'^contact/$', login_required(ContactWizard.as_view([ContactForm1, ContactForm2])), name='contact'),
    url(r'^about/$', ('home.views.about'), name="about"),
    url(r'^help/$', ('home.views.help'), name="help"),
    
   
)
