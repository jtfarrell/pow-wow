from __future__ import absolute_import

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.contrib.formtools.wizard.views import SessionWizardView
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse
from django.core.mail import send_mail

from accounts.models import UserProfile
from event.forms import ReviewForm

User = get_user_model()

@login_required
def home(request):

    user_profile = UserProfile.objects.get(user=request.user)
    
    args = {}
    args.update(csrf(request))
    args['user_info'] = user_profile
    
    return render(request, 'home.html', args)

@login_required
def users(request, user_id):

    user = UserProfile.objects.get(user=user_id)
    
    if user.user == request.user:
        return HttpResponseRedirect(reverse('home'))
    
    args = {}
    args.update(csrf(request))
    args['user_name'] = User.objects.get(id=user_id).name
    args['user_info'] = user
    
    return render(request, 'users.html', args)

@login_required
def about(request):
    
    return render(request, 'about.html')

@login_required
def help(request):
    
    return render(request, 'help.html')
    

class ContactWizard(SessionWizardView):
    
    template_name = 'contact_form.html'
    
    def done(self, form_list, **kwargs):
        
        form_data = process_form_list(form_list)
        
        messages.add_message(self.request, messages.INFO, "Message sent. We'll\
        answer as soon as possible")
        
        return render(self.request, 'contact_done.html')


def process_form_list(form_list):
    
    form_data = [form.cleaned_data for form in form_list]
    
    contact_info = form_data[0]
    message_details = form_data[1]
    
    email = 'james.t.farrell91@gmail.com'
    email_subject = '[Pow Wow] contact & support.'
    email_body = "%s \n -- %s" % (message_details, contact_info)
    
    send_mail(email_subject, email_body, email,
        [email], fail_silently=False)
    

