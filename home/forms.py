from __future__ import absolute_import

from django import forms


class ContactForm1(forms.Form):
    
    name = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'placeholder': 'John Smith'}))
    email = forms.EmailField(max_length=254, widget=forms.TextInput(attrs={'placeholder': 'jsmith@xxx.com'}))


class ContactForm2(forms.Form):
    
    body = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Include As Much Info as Possible!'}))
    
