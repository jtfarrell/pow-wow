from __future__ import absolute_import

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.core.context_processors import csrf

from friendship.models import FriendshipRequest, Friend, Follow

User = get_user_model()

def request_friendship(request, user_id):

    user = User.objects.get(id=user_id)
    Friend.objects.add_friend(request.user, user)
    messages.add_message(request, messages.SUCCESS, "Friend request sent")
    
    return HttpResponseRedirect(reverse("users", kwargs={
            'user_id': user_id}))

def accept_friendship(request, friendship_request_id):

    accept_request = FriendshipRequest.objects.get(pk=friendship_request_id)
    
    accept_request.accept()
    return HttpResponseRedirect(reverse("friendship:requests", kwargs={
                                        "user_id": accept_request.to_user_id}))

def reject_friendship(request, friendship_request_id):
    
    reject_request = FriendshipRequest.objects.get(pk=friendship_request_id)
    reject_request.reject()
    reject_request.delete()
    return HttpResponseRedirect(reverse("friendship:requests", kwargs={
                                        "user_id": accept_request.to_user_id}))

def friendship_list(request, user_id, user_name):
    pass

def delete_friendship(request, user_id):
    
    user = User.objects.get(id=user_id)
    Friend.objects.remove_friend(user, request.user)
    
    return HttpResponseRedirect(reverse("users", kwargs={
                                        "user_name": user.name,
                                        "user_id": user.id}))

def friend_request(request, user_id):
    
    friendRequests = FriendshipRequest.objects.filter(to_user=request.user)
    
    args = {}
    args.update(csrf(request))
    
    args['friend_requests'] = [i for i in friendRequests]
    
    return render(request, 'friend_requests.html', args)

