from django.conf.urls import patterns, url

urlpatterns = patterns('Friendships.views',
    url(r'^requests/(?P<user_id>\d+)/', 'friend_request', name='requests'),
    url(r'^request_friendship/(?P<user_id>\d+)', 'request_friendship', name='request_friendship'),
    url(r'^accept_friendship/(?P<friendship_request_id>\d+)', 'accept_friendship', name='accept_friendship'),
    url(r'^reject_friendship/(?P<friendship_request_id>\d+)', 'reject_friendship', name='reject_friendship'),
    url(r'^friend_list/(?P<user_id>\d+)-(?P<user_name>[-\w]+)/', 'friendship_list', name='friend_list'),
    url(r'^delete_friendship/(?P<user_id>\d+)/', 'delete_friendship', name='delete_friendship'),
)
